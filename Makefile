# Makefile
# Copyright (C) 2015
# Authors: Arun I
# 
# This file is part of ppgen - a passphrase generator
# 
# ppgen is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# ppgen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with ppgen.  If not, see <http://www.gnu.org/licenses/>.

# Load system ppgen and execute main function, pass on all
# arguments to this script to the actual ppgen main function

PROJECT=ppgen
COMMON_LISP_SOURCE_DIRECTORY=/usr/share/common-lisp/source
ASDF_SOURCE_REGISTRY=/usr/share/common-lisp/systems
BIN_DIRECTORY=/usr/bin

.PHONY: install uninstall
install:
	mkdir -p ${COMMON_LISP_SOURCE_DIRECTORY}/${PROJECT}
	install -m644 ${PROJECT}.lisp ${PROJECT}.asd cracklib-small ${COMMON_LISP_SOURCE_DIRECTORY}/${PROJECT}
	ln -s ${COMMON_LISP_SOURCE_DIRECTORY}/${PROJECT}/${PROJECT}.asd ${ASDF_SOURCE_REGISTRY}/${PROJECT}.asd
	install -m755 ${PROJECT} ${BIN_DIRECTORY}

uninstall:
	rm -rf ${COMMON_LISP_SOURCE_DIRECTORY}/${PROJECT}
	rm -f ${ASDF_SOURCE_REGISTRY}/${PROJECT}.asd
	rm -f ${BIN_DIRECTORY}/${PROJECT}
