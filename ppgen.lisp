;;; ppgen.lisp
;;; Copyright (C) 2015
;;; Authors: Arun I
;;; 
;;; This file is part of ppgen - a passphrase generator
;;; 
;;; ppgen is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;; 
;;; ppgen is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;; 
;;; You should have received a copy of the GNU General Public License
;;; along with ppgen.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :ppgen
  (:use :cl :unix-options)
  (:export :main))

(in-package :ppgen)

(defparameter *default-rng* "/dev/random")	; Path to default random number generator file
(defparameter *default-wordlist* (asdf:system-relative-pathname 'ppgen "cracklib-small"))	; Path to default wordlist
(defparameter *rnbits* 32)			; Size (in bits) of the generated random numbers

(defun random-natural-numbers (n rmax rng)
  "Return N random numbers between 1 and RMAX and inclusive of both reading the random number generator RNG"
    (with-open-file (stream rng :element-type `(unsigned-byte ,*rnbits*))
      (let ((nlim (1- (* (floor (expt 2 *rnbits*) rmax) rmax))) (x 0))
	(loop repeat n collecting 
	      (loop do (setf x (read-byte stream)) while (> x nlim) finally (return (1+ (mod x rmax))))))))

(defun number-of-lines (filename)
  "Return the number of lines in file with path FILENAME"
  (with-open-file (stream filename)
    (loop while (read-line stream nil) counting t)))

(defun get-line (filename lineno)
  "Return line number LINENO from file with path FILENAME"
  (with-open-file (stream filename)
    (loop repeat (1- lineno) do (read-line stream)
	  finally (return (read-line stream)))))

(defun get-words (nwords filename rng)
  "Return NWORDS number of words from file with path FILENAME"
  (let ((nlines (number-of-lines filename)))
    (loop for lineno in (random-natural-numbers nwords nlines rng) collecting (get-line filename lineno))))

(defun main (argv)
  (with-cli-options
    (argv "~%Usage: ppgen [OPTIONS] PASSPHRASE-LENGTH~2%~@{~a~%~}~%")
    (&parameters (wordlist "Path to wordlist file from which to randomly pick words")
		 (rng "Path to random number generator [default: /dev/random]")
		 &free passphrase-length)
    (let ((nwords (read-from-string (first passphrase-length)))
	  (wordlist (if (not wordlist) *default-wordlist* wordlist))
	  (rng (if (not rng) *default-rng* rng)))
      (format t "~%~{~a~t~}~2%" (get-words nwords wordlist rng)))))
