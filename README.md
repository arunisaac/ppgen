ppgen
=====
ppgen is a passphrase generator program that generate cryptographically
secure passphrases.

To know more about passphrases, please consult the Wikipedia page

https://en.wikipedia.org/wiki/Passphrase

and/or the XKCD comic strip

https://xkcd.com/936/

By default, ppgen reads */dev/random* to obtain random numbers. */dev/random*
is used because it is a cryptographically secure pseudo random number
generator and provides output of large entropy.

However, if you wish to use some other random number generator (say, a
hardware random number generator */dev/hwrng*), you may do so by using the
appropriate command line option.

Wordlist
--------
ppgen comes with the small version of the cracklib dictionary, and this
is used by default. For best results, you should use as large a wordlist
as possible. You can download the full cracklib wordlist from

http://prdownloads.sourceforge.net/cracklib/cracklib-words.gz?download

Other sources for wordlists include:

http://wordlist.sourceforge.net/
http://www.openwall.com/wordlists/

Installation
------------
ppgen is written in Common Lisp and depends on

* unix-options (http://www.cliki.net/unix-options)
* cl-launch (http://cliki.net/CL-Launch)

ppgen has been tested in SBCL, CLISP and CMUCL.

In order to get the paths right, ppgen needs to be installed into the
correct asdf and common-lisp source directories on your system before
it can be used.

A Makefile is provided for this purpose. Set the correct paths for your
system in the variables *COMMON_LISP_SOURCE_DIRECTORY*, *ASDF_SOURCE_REGISTRY*,
and *BIN_DIRECTORY*, and then execute the appropriate make commands.

To install, type

```
# make install
```

To uninstall, type

```
# make uninstall
```

Usage
-----
For a summary of the command line options, type

```
$ ppgen -h
```

To generate a passphrase of 6 words, type

```
$ ppgen 6
```

To use a non-default wordlist and generate a passphrase of 8 words, type

```
$ ppgen -w path-to-your-wordlist 8
```

In general, the usage format is

```
$ ppgen -w path-to-wordlist passphrase-length
```

License
-------
ppgen is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The default wordlist is from the cracklib project, and is subject to
their license.

http://cracklib.sourceforge.net/
